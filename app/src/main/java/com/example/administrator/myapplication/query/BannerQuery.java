package com.example.administrator.myapplication.query;

import com.example.administrator.myapplication.model.Banner;

import java.util.List;

/**
 * Created by Administrator on 7/5/2016.
 */
public abstract class BannerQuery {
    public abstract List<Banner> queryBanners() throws Exception;
}
