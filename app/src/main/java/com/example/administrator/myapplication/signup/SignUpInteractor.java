package com.example.administrator.myapplication.signup;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface SignUpInteractor {

    interface Callback {
        void onSignUpStarted();
        void onSignUpCompleted(String username, Account account, int message);
        void onSignUpError();
    }


    void signUp(String username, String password, String email, Callback callback);
}
