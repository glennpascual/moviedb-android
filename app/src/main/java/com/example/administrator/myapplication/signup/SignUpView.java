package com.example.administrator.myapplication.signup;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface SignUpView {

    String getUsername();
    String getPassword();

    void showProgress();
    void hideProgress();

    void setUserNameErrorMessage(String message);
    void setPasswordErrorMessage(String message);

    void finishSignIn(Account account);


}
