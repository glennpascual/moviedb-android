package com.example.administrator.myapplication.login;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface LoginPresenter {

    void login(String username, String password);
    void onDestroy();

}
