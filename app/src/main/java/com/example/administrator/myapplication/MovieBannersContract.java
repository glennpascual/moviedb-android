package com.example.administrator.myapplication;

import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;

import java.util.List;

/**
 * Created by Administrator on 7/13/2016.
 */
public interface MovieBannersContract {

    interface View {

        void showProgressBarMovies();
        void hideProgressBarMovies();

        void showProgressBarBanners();
        void hideProgressBarBanners();


        void setMovies(List<Movie> movies);
        void setBanners(List<Banner> movies);

    }

    interface Presenter {

        void loadMovies();
        void loadBanners();

        void onDestroy();
        void onResume();
        void onPause();

    }
}
