package com.example.administrator.myapplication.login.presenter;

import android.content.Context;

import com.example.administrator.myapplication.login.LoginInteractor;
import com.example.administrator.myapplication.login.LoginPresenter;
import com.example.administrator.myapplication.login.LoginView;
import com.example.administrator.myapplication.login.interactor.LoginInterActorImpl;
import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.threadpool.TaskManager;

/**
 * Created by Administrator on 7/8/2016.
 */
public class LoginPresenterImpl
    implements
        LoginPresenter,
        LoginInteractor.Callback {


    private LoginView loginView;
    private Context context;
    private TaskManager taskManager;

    // Interactor
    private LoginInteractor loginInteractor;


    public LoginPresenterImpl(LoginView loginView, Context context, TaskManager taskManager) {
        this.loginView = loginView;
        this.context = context;
        this.taskManager = taskManager;

        loginInteractor = new LoginInterActorImpl(taskManager);
    }


    @Override
    public void login(String username, String password) {

        if(username == null || username.isEmpty()) {
            loginView.setUserNameErrorMessage("Username cannot be empty");
            return;
        }

        if(password == null || password.isEmpty()) {
            loginView.setPasswordErrorMessage("Password cannot be empty");
            return;
        }

        //
        loginInteractor.login(username, password, this);


    }

    @Override
    public void onDestroy() {
        this.context = null;
        this.loginView = null;
    }


    //
    // LoginInteractor.Callback implementation


    @Override
    public void onLoginStarted() {
        loginView.showProgress();
    }

    @Override
    public void onLoginCompleted(String username, Account account, int message) {
        loginView.hideProgress();
        loginView.finishLogin(account);
    }

    @Override
    public void onLoginError() {
        loginView.hideProgress();
    }


}
