package com.example.administrator.myapplication.login;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface LoginView {



    String getUsername();
    String getPassword();

    void showProgress();
    void hideProgress();

    void setUserNameErrorMessage(String message);
    void setPasswordErrorMessage(String message);

    void finishLogin(Account account);



}
