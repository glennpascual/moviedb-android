package com.example.administrator.myapplication.model;

/**
 * Created by Administrator on 7/5/2016.
 */
public class Banner {


    //
    private Movie movieBanner;


    public Banner(Movie movieBanner) {
        this.movieBanner = movieBanner;
    }

    public Movie getMovieBanner() {
        return movieBanner;
    }
}
