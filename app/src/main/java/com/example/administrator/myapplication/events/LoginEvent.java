package com.example.administrator.myapplication.events;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/19/2016.
 */
public class LoginEvent
    extends AuthenticationEvent {


    public LoginEvent(Account account) {
        super(account);
    }
}
