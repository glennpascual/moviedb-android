package com.example.administrator.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.model.Account;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 7/8/2016.
 */
public class ProfileFragment
    extends BaseFragment {



    private View inflatedView;

    @BindView(R.id.content) View content;
    @BindView(R.id.textViewPleaseLoginMessage) TextView textViewLoginMessage;


    @BindView(R.id.imageViewProfileImage) ImageView profileImage;

    @BindView(R.id.textViewUsername) TextView textViewUsername;
    @BindView(R.id.textViewFullname) TextView textViewFullName;
    @BindView(R.id.textViewAddress) TextView textViewAddress;


    //
    private Account account;

    //
    private Picasso picasso;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, inflatedView);

        picasso = Picasso.with(getActivity());
        return inflatedView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(account != null)
            populateProfile(account);

    }


    @Override
    public String getFragmentTitle() {
        return "Profile";
    }

    private void populateProfile(Account account) {

        if(account != null) {

            textViewLoginMessage.setVisibility(View.INVISIBLE);
            content.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(account.getUserImageURI()))
                picasso.load(account.getUserImageURI()).into(profileImage);

            // Populate
            textViewUsername.setText("@" + account.getUserName());
            textViewFullName.setText(account.getFullName());
            textViewAddress.setText(account.getAddress());


        }

        else {

            textViewLoginMessage.setVisibility(View.VISIBLE);

            content.setVisibility(View.INVISIBLE);

            profileImage.setImageBitmap(null);

            // Populate
            textViewUsername.setText("");
            textViewFullName.setText("");
            textViewAddress.setText("");

        }


    }


    public void setAccount(Account account) {
        this.account = account;

        if(inflatedView != null) {
            populateProfile(account);
        }

    }


}
