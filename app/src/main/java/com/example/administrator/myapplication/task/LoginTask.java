package com.example.administrator.myapplication.task;

import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.query.LoginQuery;
import com.example.administrator.myapplication.query.RandomUserLoginQuery;
import com.example.administrator.myapplication.threadpool.ManagedAsyncTask;

/**
 * Created by Administrator on 7/8/2016.
 */
public class LoginTask
        extends ManagedAsyncTask<Void, LoginTask.LoginResponse> {


    // Authentication Message codes
    public static final int MESSAGE_SUCCESS = 0;
    public static final int MESSAGE_BADCOMBINATION = 1;
    public static final int MESSAGE_ACCOUNT_DISABLED = 2;
    public static final int MESSAGE_ACCOUNT_BANNED = 3;
    public static final int MESSAGE_ACCOUNT_USERNAME_NOT_EXISTS = 4;
    public static final int MESSAGE_ACCOUNT_USERNAME_NEED_VERIFICATION = 5;


    private String username;
    private String password;


    // Callback
    private Callback callback;


    public interface Callback {
        void onLoginStarted(String username);

        void onLoginCompleted(int messageCode, String username, Account account);

        void onLoginError();
    }


    //
    // Model class as Response of login
    static class LoginResponse {

        Account account;
        int messageCode;

    }

    public LoginTask(String username, String password) {

        if (username == null || username.isEmpty())
            throw new IllegalArgumentException("Username cannot be empty or null");

        this.username = username;
        this.password = password;
    }


    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (callback != null)
            callback.onLoginStarted(username);
    }


    @Override
    public LoginResponse standByDoInBackground(Object... objects) {


        try {

            LoginQuery loginQuery = new RandomUserLoginQuery();

            Account account = loginQuery.login(username, password);


            LoginResponse loginResponse = new LoginResponse();
            loginResponse.account = account;

            //
            loginResponse.messageCode = MESSAGE_SUCCESS;

            //
            return loginResponse;

        }
        catch (Exception e) {
            e.printStackTrace();

            return null;
        }



    }

    @Override
    protected void onPostExecute(LoginResponse loginResponse) {
        super.onPostExecute(loginResponse);

        if (callback != null) {
            if (loginResponse != null)
                callback.onLoginCompleted(loginResponse.messageCode, username, loginResponse.account);
            else
                callback.onLoginError();
        }

    }
}
