package com.example.administrator.myapplication;

import android.app.Application;
import android.content.Context;

import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.threadpool.TaskManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MovieDBApplication
    extends Application {


    private static final int USERSESSION_THREADPOOL_SIZE = 3;
    private static final int APPLICATION_THREADPOOL_SIZE = 2;




    // TODO Convert to Map<Account, UserSession>
    // List of user sessions
    private List<UserSession> userSessionList;

    // Current user session
    private UserSession currentUserSession;

    //
    private Map<UserSession, TaskManager> userSessionTaskManager;


    // Application's Task Manager
    private TaskManager applicationTaskManager;


    public static MovieDBApplication from(Context context) {
        return (MovieDBApplication) context.getApplicationContext();
    }


    @Override
    public void onCreate() {
        super.onCreate();


        //
        // Initialize user session list
        userSessionList = new ArrayList<UserSession>();

        //
        //
        userSessionTaskManager = new HashMap<UserSession, TaskManager>();


    }

    public List<UserSession> getUserSessionList() {
        return userSessionList;
    }

    public UserSession getCurrentUserSession() {
        return currentUserSession;
    }

    public TaskManager getTaskManagerForUserSession(UserSession userSession) {

        if(userSession == null)
            return null;

        // If
        TaskManager taskManager = userSessionTaskManager.get(userSession);

        if(taskManager == null) {
            taskManager = new TaskManager(USERSESSION_THREADPOOL_SIZE);
            userSessionTaskManager.put(userSession, taskManager);
        }

        return taskManager;

    }


    public TaskManager getApplicationTaskManager() {

        if(applicationTaskManager == null)
            applicationTaskManager = new TaskManager(APPLICATION_THREADPOOL_SIZE);

        return applicationTaskManager;
    }

    public boolean addUserSession(UserSession userSession) {

        if(userSession == null)
            throw new IllegalArgumentException("User session needed");

        //
        userSessionList.add(userSession);

        //
        return true;

    }


    public boolean removeSession(UserSession userSession) {

        if(userSession == null)
            throw new IllegalArgumentException("User session needed");

        //
        if(currentUserSession == userSession)
            currentUserSession = null;

        //
        userSessionList.remove(userSession);

        return false;

    }


    public void setCurrentUserSession(UserSession userSession) {
        this.currentUserSession = userSession;
    }



}
