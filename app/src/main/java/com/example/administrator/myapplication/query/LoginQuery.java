package com.example.administrator.myapplication.query;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/8/2016.
 */
public abstract class LoginQuery {


    public abstract Account login(String username, String password) throws Exception;

}
