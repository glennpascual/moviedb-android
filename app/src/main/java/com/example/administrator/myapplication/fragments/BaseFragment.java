package com.example.administrator.myapplication.fragments;

import android.support.v4.app.Fragment;

/**
 * Created by Administrator on 7/4/2016.
 */
public class BaseFragment
    extends Fragment {


    private String fragmentTitle;
    private String backStackName;

    public void setFragmentTitle(String fragmentTitle) {
        this.fragmentTitle = fragmentTitle;
    }

    public String getFragmentTitle() {
        return fragmentTitle;
    }


    public String getBackStackName() {
        return backStackName;
    }

    public void setBackStackName(String backStackName) {
        this.backStackName = backStackName;
    }


}
