package com.example.administrator.myapplication.movie.detail;

import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.threadpool.TaskManager;

/**
 * Created by Administrator on 7/11/2016.
 */
public class MovieDetailPresenter
    implements MovieDetailContract.Presenter {


    // Task name constants
    private static final String TASK_NAME_MOVIE_DETAIL_FETCH = "TASK_NAME_MOVIE_DETAIL_FETCH";

    //
    private TaskManager taskManager;

    //
    //
    private MovieDetailFetchTask movieDetailFetchTask;

    //
    // View
    private MovieDetailContract.View movieDetailView;


    public MovieDetailPresenter(MovieDetailContract.View movieDetailView,
                                TaskManager taskManager) {
        this.taskManager = taskManager;
        this.movieDetailView = movieDetailView;
    }

    @Override
    public void onDestroy() {

        movieDetailView = null;

        if(movieDetailFetchTask != null)
            movieDetailFetchTask.setCallback(null);

    }

    @Override
    public void loadMovie(int movieId) {

        movieDetailFetchTask = new MovieDetailFetchTask(movieId, null);
        movieDetailFetchTask.setCallback(new MovieDetailFetchTask.Callback() {
            @Override
            public void onMovieDetailFetchStart() {
                if(movieDetailView != null)
                    movieDetailView.showProgress();
            }

            @Override
            public void onMovieDetailFetchCompleted(UserSession userSession, Movie movie) {
                if(movieDetailView != null) {
                    movieDetailView.hideProgress();
                    movieDetailView.setMovie(movie);
                }
            }

            @Override
            public void onMovieDetailFetchError() {
                if(movieDetailView != null)
                    movieDetailView.hideProgress();
            }
        });

        //
        // Execute in task manage
        taskManager.executeTask(TASK_NAME_MOVIE_DETAIL_FETCH, movieDetailFetchTask);

    }
}
