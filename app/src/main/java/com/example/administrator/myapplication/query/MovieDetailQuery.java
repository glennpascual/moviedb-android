package com.example.administrator.myapplication.query;

import com.example.administrator.myapplication.model.Movie;

import java.io.Closeable;

/**
 * Created by Administrator on 7/6/2016.
 */
public abstract class MovieDetailQuery
    implements Closeable {

    public abstract Movie fetchMovie(int movieId) throws Exception;
}
