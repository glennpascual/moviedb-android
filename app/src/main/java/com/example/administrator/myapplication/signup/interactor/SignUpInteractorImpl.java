package com.example.administrator.myapplication.signup.interactor;

import android.os.AsyncTask;

import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.signup.SignUpInteractor;
import com.example.administrator.myapplication.task.SignUpTask;
import com.example.administrator.myapplication.threadpool.TaskManager;

/**
 * Created by Administrator on 7/8/2016.
 */
public class SignUpInteractorImpl
    implements SignUpInteractor {


    // Task name constants
    private static final String TASK_NAME_SIGNUP = "signup_task";


    // Task manager
    private TaskManager taskManager;

    private Callback callback;

    //
    private SignUpTask signUpTask;
    private SignUpTask.Callback signUpTaskCallback;


    public SignUpInteractorImpl(TaskManager taskManager) {
        this.taskManager = taskManager;
    }

    @Override
    public void signUp(String username, String password, String email, final Callback callback) {

        //
        signUpTask = (SignUpTask) taskManager.getRunningTask(TASK_NAME_SIGNUP);

        if(signUpTask == null || signUpTask.getStatus() == AsyncTask.Status.FINISHED) {

            signUpTaskCallback = new SignUpTask.Callback() {
                @Override
                public void onSignUpStarted(String username) {
                    if(callback != null)
                        callback.onSignUpStarted();
                }

                @Override
                public void onSignUpCompleted(int messageCode, String username, Account account) {
                    if(callback != null)
                        callback.onSignUpCompleted(username, account, messageCode);
                }

                @Override
                public void onSignUpError() {
                    if(callback != null)
                        callback.onSignUpError();
                }
            };

            signUpTask = new SignUpTask(username, password);
            signUpTask.setCallback(signUpTaskCallback);

            // Execute in Task manager
            taskManager.executeTask(TASK_NAME_SIGNUP, signUpTask);

        }
        else {
            signUpTask.setCallback(signUpTaskCallback);
        }

    }
}
