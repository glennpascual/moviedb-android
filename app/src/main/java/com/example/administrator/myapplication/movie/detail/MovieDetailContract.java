package com.example.administrator.myapplication.movie.detail;

import com.example.administrator.myapplication.model.Movie;

/**
 * Created by Administrator on 7/11/2016.
 */
public interface MovieDetailContract {

    interface View {

        void showProgress();
        void setMovie(Movie movie);
        void hideProgress();

    }

    interface Presenter {
        void loadMovie(int movieId);
        void onDestroy();
    }

    interface Interactor {
        void loadMovie(int movieId);
    }

}
