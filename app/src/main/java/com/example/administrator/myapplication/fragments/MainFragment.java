package com.example.administrator.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.administrator.myapplication.MainActivity;
import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.events.LoginEvent;
import com.example.administrator.myapplication.events.SignOutEvent;
import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.utils.TabViewUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 7/15/2016.
 */
public class MainFragment
    extends BaseFragment
    implements
        MoviesBannersFragment.Listener {



    // Back stack name constants
    private static final String BACKSTACK_NAME_MOVIEDETAIL_FRAGMENT = "backstack_name_moviedetail_fragment";


    // Tabs
    private TabLayout.Tab homeTab;
    private TabLayout.Tab profileTab;


    //
    private UserSession userSession;


    //
    @BindView(R.id.viewPagerContent) ViewPager viewPager;
    @BindView(R.id.tabLayout) TabLayout tabLayout;


    // Fragments
    private MoviesBannersFragment moviesBannersFragment;
    private ProfileFragment profileFragment;


    public MainFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);


        inflater.inflate(R.menu.menu_main_activity, menu);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, content);
        return content;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        homeTab = tabLayout.newTab().setCustomView(TabViewUtils.createTabView(getContext(), "Home", R.drawable.ic_tab_home));
        profileTab = tabLayout.newTab().setCustomView(TabViewUtils.createTabView(getContext(), "Profile", R.drawable.ic_tab_profile));

        tabLayout.addTab(homeTab);
        tabLayout.addTab(profileTab);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        // ViewPager
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {


            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }


            @Override
            public void onPageScrollStateChanged(int state) {

                //


            }


        });

        //
        //
        moviesBannersFragment = new MoviesBannersFragment();
        moviesBannersFragment.setListener(this);
        moviesBannersFragment.setFragmentTitle("Home");

        profileFragment = new ProfileFragment();
        profileFragment.setFragmentTitle("Profile");


        if(userSession != null)
            profileFragment.setAccount(userSession.getAccount());


        //
        setupContent(Arrays.<BaseFragment>asList(moviesBannersFragment, profileFragment));


    }

    private void setupContent(List<BaseFragment> baseFragments) {

        //
        ContentPagerAdapter contentPagerAdapter
                = new ContentPagerAdapter(getFragmentManager(), baseFragments);

        viewPager.setAdapter(contentPagerAdapter);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        setMenuVisibility(false);
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();

    }

    private static class ContentPagerAdapter
            extends FragmentPagerAdapter {

        private List<BaseFragment> baseFragments;

        public ContentPagerAdapter(FragmentManager fm, List<BaseFragment> baseFragments) {
            super(fm);
            this.baseFragments = baseFragments;
        }

        @Override
        public BaseFragment getItem(int position) {
            return baseFragments.get(position);
        }

        @Override
        public int getCount() {
            return baseFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return baseFragments.get(position).getFragmentTitle();
        }

    }


    //
    //
    //
    // MoviesBannersFragment.Listener implementation


    @Override
    public void onBannerItemClicked(Banner banner, int position) {
        Movie movieBanner = banner.getMovieBanner();

        Bundle arguments = new Bundle();
        arguments.putInt(MovieDetailFragment.EXTRA_MOVIEID, movieBanner.getId());

        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.setBackStackName(BACKSTACK_NAME_MOVIEDETAIL_FRAGMENT);
        movieDetailFragment.setArguments(arguments);

        ((MainActivity) getActivity()).addNavigation(movieDetailFragment, true, true);


    }

    @Override
    public void onMovieItemClicked(Movie movie, int position) {

        Bundle arguments = new Bundle();
        arguments.putInt(MovieDetailFragment.EXTRA_MOVIEID, movie.getId());

        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.setBackStackName(BACKSTACK_NAME_MOVIEDETAIL_FRAGMENT);
        movieDetailFragment.setArguments(arguments);

        ((MainActivity) getActivity()).addNavigation(movieDetailFragment, true, true);


    }


    //
    //

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginEvent(LoginEvent loginEvent) {
        profileFragment.setAccount(loginEvent.getAccount());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignOutEvent(SignOutEvent signOutEvent) {
        profileFragment.setAccount(null);
    }


}

