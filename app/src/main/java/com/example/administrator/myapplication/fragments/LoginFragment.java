package com.example.administrator.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.administrator.myapplication.MainActivity;
import com.example.administrator.myapplication.MovieDBApplication;
import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.events.LoginEvent;
import com.example.administrator.myapplication.events.SignUpEvent;
import com.example.administrator.myapplication.login.LoginPresenter;
import com.example.administrator.myapplication.login.LoginView;
import com.example.administrator.myapplication.login.presenter.LoginPresenterImpl;
import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.threadpool.TaskManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 7/15/2016.
 */
public class LoginFragment
    extends BaseFragment
    implements LoginView {


    // Login presenter
    private LoginPresenter loginPresenter;


    //
    // Edit Text
    @BindView(R.id.email) EditText editTextUsername;
    @BindView(R.id.password) EditText editTextPassword;
    @BindView(R.id.login_progress) ProgressBar progressBar;


    // Buttons
    @BindView(R.id.email_sign_in_button) Button loginButton;
    @BindView(R.id.email_sign_up_button) Button signUpButton;


    public LoginFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);

        // Register event bus
        EventBus.getDefault().register(this);

    }


    //
    //

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_login, container, false);
        ButterKnife.bind(this, content);

        return content;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MovieDBApplication movieDBApplication = MovieDBApplication.from(getContext());
        UserSession userSession = movieDBApplication.getCurrentUserSession();

        TaskManager taskManager = movieDBApplication.getApplicationTaskManager();

        loginPresenter = new LoginPresenterImpl(this, getContext(), taskManager);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Register event bus
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.email_sign_in_button)
    public void login() {
        loginPresenter.login(getUsername(), getPassword());
    }

    @OnClick(R.id.email_sign_up_button)
    public void signUp() {

        SignUpFragment signUpFragment = new SignUpFragment();
        signUpFragment.setBackStackName("signup_fragment");

        ((MainActivity) getActivity()).addNavigation(signUpFragment, true, true);
    }



    //
    // LoginView implementation

    @Override
    public String getUsername() {
        return editTextUsername.getText().toString();
    }

    @Override
    public String getPassword() {
        return editTextPassword.getText().toString();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUserNameErrorMessage(String message) {
        editTextUsername.setError(message);
    }

    @Override
    public void setPasswordErrorMessage(String message) {
        editTextPassword.setError(message);
    }

    @Override
    public void finishLogin(Account account) {

        EventBus.getDefault().post(new LoginEvent(account));

        ((MainActivity) getActivity()).popNavigation(this);
    }



    //
    // Event bus subscription

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSignUpEvent(SignUpEvent signUpEvent) {
        finishLogin(signUpEvent.getAccount());
    }



}
