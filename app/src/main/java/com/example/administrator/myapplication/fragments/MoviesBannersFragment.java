package com.example.administrator.myapplication.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.administrator.myapplication.MovieBannersContract;
import com.example.administrator.myapplication.MoviesBannerPresenter;
import com.example.administrator.myapplication.MovieDBApplication;
import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.threadpool.TaskManager;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MoviesBannersFragment
    extends BaseFragment
    implements MovieBannersContract.View {


    // State constants
    private static final String STATE_BANNERINDICATOR_VISIBLE = "state_bannerindicator_visible";
    private static final String STATE_MOVIEINDICATOR_VISIBLE = "state_movieindicator_visible";


    //
    private ViewPager viewPager;
    private View progressIndicatorBanner;
    private TextView textViewIndicatorMessageBanner;
    private ProgressBar progressBarBanner;

    //
    private ListView listView;
    private View progressIndicatorMovies;
    private TextView textViewIndicatorMessageMovies;
    private ProgressBar progressBarMovies;


    // Listener
    private Listener listener;

    // Displayed List of movies
    private List<Movie> movieList;

    // Displayed List of Banners
    private List<Banner> bannerList;

    //
    // Presenter
    private MovieBannersContract.Presenter presenter;



    public interface Listener {
        void onBannerItemClicked(Banner banner, int position);
        void onMovieItemClicked(Movie movie, int position);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        //
        // Retrieve saved instance states
        View content = inflater.inflate(R.layout.fragment_moviesbanners, container, false);

        viewPager = (ViewPager) content.findViewById(R.id.viewPagerBanner);
        progressIndicatorBanner = content.findViewById(R.id.progressIndicatorBanner);

        progressBarBanner = (ProgressBar) progressIndicatorBanner.
                                            findViewById(R.id.progressBarIndicatorProgress);
        textViewIndicatorMessageBanner = (TextView) progressIndicatorBanner.
                                            findViewById(R.id.textViewIndicatorMessage);



        listView = (ListView) content.findViewById(R.id.listViewMovies);
        progressIndicatorMovies = content.findViewById(R.id.progressIndicatorMovies);

        progressBarMovies = (ProgressBar) progressIndicatorMovies.
                                            findViewById(R.id.progressBarIndicatorProgress);
        textViewIndicatorMessageMovies = (TextView) progressIndicatorBanner.
                                            findViewById(R.id.textViewIndicatorMessage);



        Log.d("MoviesBannersFragment", "onCreateView()");
        return content;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MovieDBApplication movieDBApplication = MovieDBApplication.from(getContext());

        presenter = new MoviesBannerPresenter(this, movieDBApplication.getApplicationTaskManager());

        presenter.loadBanners();
        presenter.loadMovies();

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        presenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    private void populateMovies(List<Movie> movieList) {
        listView.setAdapter(new MoviesListAdapter(movieList));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            if(listener != null) {
                listener.onMovieItemClicked((Movie) adapterView.getItemAtPosition(i), i);
            }

            }
        });

    }


    private void populateBanners(List<Banner> banners) {

        BannerPagerAdapter bannerPagerAdapter = new BannerPagerAdapter(getChildFragmentManager(), banners);
        bannerPagerAdapter.setListener(new BannerFragment.Listener() {
            @Override
            public void onBannerClicked(Banner banner) {
            if(listener != null)
                listener.onBannerItemClicked(banner, -1);
            }
        });

        viewPager.setAdapter(bannerPagerAdapter);

    }


    //
    // MovieBannersContract.View


    @Override
    public void showProgressBarMovies() {
        progressIndicatorMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBarMovies() {
        progressIndicatorMovies.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showProgressBarBanners() {
        progressIndicatorBanner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBarBanners() {
        progressIndicatorBanner.setVisibility(View.INVISIBLE);
    }


    @Override
    public void setMovies(List<Movie> movies) {
        this.movieList = movies;
        populateMovies(movies);
    }

    @Override
    public void setBanners(List<Banner> banners) {
        this.bannerList = banners;
        populateBanners(banners);
    }


    //
    //
    private static class MoviesListAdapter
        extends BaseAdapter {

        //
        private Picasso picasso;

        //
        private List<Movie> movies;

        // Layout inflater
        private LayoutInflater layoutInflater;



        public MoviesListAdapter(List<Movie> movies) {

            if(movies == null)
                throw new IllegalArgumentException("List of Movies needed");

            this.movies = movies;
        }

        @Override
        public int getCount() {
            return movies.size();
        }

        @Override
        public Movie getItem(int i) {
            return movies.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            Context context = viewGroup.getContext();

            Movie item = getItem(i);

            ViewHolder viewHolder = null;

            if(picasso == null)
                picasso = Picasso.with(context);

            if(view == null) {

                if(layoutInflater == null)
                    layoutInflater = LayoutInflater.from(context);

                viewHolder = new ViewHolder();
                view = layoutInflater.inflate(R.layout.list_item_movie, viewGroup, false);
                viewHolder.imageViewMovie = (ImageView) view.findViewById(R.id.imageViewMovie);
                viewHolder.title = (TextView) view.findViewById(R.id.textViewTitle);
                viewHolder.category = (TextView) view.findViewById(R.id.textViewCategory);


                view.setTag(viewHolder);

            }
            else {
                viewHolder = (ViewHolder) view.getTag();
            }

            picasso.load(item.getMoviePosterURI()).into(viewHolder.imageViewMovie);
            viewHolder.title.setText(item.getTitle());
            viewHolder.category.setText(item.getCategory());


            return view;
        }

        private static class ViewHolder {
            ImageView imageViewMovie;
            TextView title;
            TextView category;
        }

    }


    //
    private static class BannerPagerAdapter
        extends FragmentPagerAdapter
        implements BannerFragment.Listener {


        //
        private List<Banner> banners;

        //
        private BannerFragment.Listener listener;


        public BannerPagerAdapter(FragmentManager fm, List<Banner> banners) {
            super(fm);
            this.banners = banners;
        }

        @Override
        public Fragment getItem(int position) {

            BannerFragment bannerFragment = new BannerFragment();
            bannerFragment.setBanner(banners.get(position));
            bannerFragment.setListener(this);

            return bannerFragment;
        }

        @Override
        public int getCount() {
            return banners.size();
        }

        public void setListener(BannerFragment.Listener listener) {
            this.listener = listener;
        }

        @Override
        public void onBannerClicked(Banner banner) {
            if(listener != null)
                listener.onBannerClicked(banner);
        }

    }


}
