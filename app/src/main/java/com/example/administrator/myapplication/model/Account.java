package com.example.administrator.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 7/4/2016.
 */
public class Account
    implements Parcelable {


    private String userName;
    private String token;

    private String userImageURI;

    //
    private String fullName;
    private String address;


    public Account(String userName, String token) {
        this.userName = userName;
        this.token = token;
    }

    protected Account(Parcel in) {
        userName = in.readString();
        token = in.readString();
        userImageURI = in.readString();
        fullName = in.readString();
        address = in.readString();
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserName() {
        return userName;
    }

    public String getToken() {
        return token;
    }

    public String getUserImageURI() {
        return userImageURI;
    }

    public void setUserImageURI(String userImageURI) {
        this.userImageURI = userImageURI;
    }

    public String getFullName() {
        return fullName;
    }

    public String getAddress() {
        return address;
    }

    //
    // Parcelable implementation


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(token);
        dest.writeString(userImageURI);
        dest.writeString(fullName);
        dest.writeString(address);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Account> CREATOR = new Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };

    //
    //

}
