package com.example.administrator.myapplication.threadpool;

import android.os.AsyncTask;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Administrator on 7/4/2016.
 */
public abstract class ManagedAsyncTask<Param, Result>
    extends AsyncTask<Object, Param, Result> {


    // Standby Flag constants
    public static final int STANDBY_NONE = 1;
    public static final int STANDBY_BEFORE_EXECUTION = 2;
    public static final int STANDBY_AFTER_EXECUTION = 4;


    private String taskName;
    private TaskLifeCycleListener taskLifeCycleListener;


    // Signal for wait.
    private AtomicBoolean atomicStandBy;

    String getTaskName() {
        return taskName;
    }

    void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    void setTaskLifeCycleListener(TaskLifeCycleListener taskLifeCycleListener) {
        this.taskLifeCycleListener = taskLifeCycleListener;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(atomicStandBy == null)
            atomicStandBy = new AtomicBoolean(false);

        if(taskLifeCycleListener != null)
            taskLifeCycleListener.onCreate(taskName);

    }


    @Override
    protected void onCancelled() {
        super.onCancelled();

        if(taskLifeCycleListener != null)
            taskLifeCycleListener.onDeath(taskName);
    }

    @Override
    protected void onCancelled(Result result) {
        super.onCancelled(result);

        if(taskLifeCycleListener != null)
            taskLifeCycleListener.onDeath(taskName);
    }

    public abstract Result standByDoInBackground(Object... objects);

    @Override
    protected final Result doInBackground(Object... objects) {

        while(atomicStandBy.get());
        Result result = standByDoInBackground();
        while(atomicStandBy.get());

        return result;
    }

    public boolean isStandingBy() {
        return atomicStandBy.get();
    }

    public void standBy(boolean standby) {

        if(atomicStandBy == null)
            atomicStandBy = new AtomicBoolean(standby);
        else
            atomicStandBy.set(standby);



    }


    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(taskLifeCycleListener != null)
            taskLifeCycleListener.onDeath(taskName);
    }
}
