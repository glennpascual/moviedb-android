package com.example.administrator.myapplication.task;

import android.os.SystemClock;

import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.threadpool.ManagedAsyncTask;

/**
 * Created by Administrator on 7/8/2016.
 */
public class SignUpTask
        extends ManagedAsyncTask<Void, SignUpTask.LoginResponse> {


    // Authentication Message codes
    public static final int MESSAGE_SUCCESS = 0;
    public static final int MESSAGE_EMAIL_ALREDY_USED = 1;


    private String username;
    private String password;


    // Callback
    private Callback callback;


    public interface Callback {
        void onSignUpStarted(String username);

        void onSignUpCompleted(int messageCode, String username, Account account);

        void onSignUpError();
    }


    //
    // Model class as Response of signUp
    static class LoginResponse {

        Account account;
        int messageCode;

    }

    public SignUpTask(String username, String password) {

        if (username == null || username.isEmpty())
            throw new IllegalArgumentException("Username cannot be empty or null");

        this.username = username;
        this.password = password;
    }


    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (callback != null)
            callback.onSignUpStarted(username);
    }


    @Override
    public LoginResponse standByDoInBackground(Object... objects) {

        try {

            // A little delay. 3 Seconds
            SystemClock.sleep(3000);

            // A mock authentication token. Hash code of username as Hexadecimal
            String token = String.valueOf(Integer.toHexString(username.hashCode()));

            Account account = new Account(username, token);

            LoginResponse loginResponse = new LoginResponse();
            loginResponse.account = account;

            //
            loginResponse.messageCode = MESSAGE_SUCCESS;

            //
            return loginResponse;

        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }


    }



    @Override
    protected void onPostExecute(LoginResponse loginResponse) {
        super.onPostExecute(loginResponse);

        if (callback != null) {
            if (loginResponse != null)
                callback.onSignUpCompleted(loginResponse.messageCode, username, loginResponse.account);
            else
                callback.onSignUpError();
        }

    }
}
