package com.example.administrator.myapplication.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.myapplication.R;

/**
 * Created by Administrator on 7/14/2016.
 */
public class TabViewUtils {


    public static View createTabView(Context context, CharSequence text, int iconId) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View tabContent = layoutInflater.inflate(R.layout.layout_tab, null);

        ImageView imageViewIcon = (ImageView) tabContent.findViewById(R.id.icon);
        TextView textViewCaption = (TextView) tabContent.findViewById(R.id.caption);


        if(iconId > 0) {
            imageViewIcon.setImageResource(iconId);
            imageViewIcon.setVisibility(View.VISIBLE);
        }
        else
            imageViewIcon.setVisibility(View.GONE);

        //
        //
        if(text == null)
            textViewCaption.setText("");
        else
            textViewCaption.setText(text);

        //
        return tabContent;

    }


    public static View createTabView(Context context, int captionId, int iconId) {

        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View tabContent = layoutInflater.inflate(R.layout.layout_tab, null);

        ImageView imageViewIcon = (ImageView) tabContent.findViewById(R.id.icon);
        TextView textViewCaption = (TextView) tabContent.findViewById(R.id.caption);


        if(iconId > 0) {
            imageViewIcon.setImageResource(iconId);
            imageViewIcon.setVisibility(View.VISIBLE);
        }
        else
            imageViewIcon.setVisibility(View.GONE);

        if(captionId > 0)
            textViewCaption.setText(captionId);
        else
            textViewCaption.setText("");


        return tabContent;

    }
}
