package com.example.administrator.myapplication.events;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/19/2016.
 */
class AuthenticationEvent {

    private Account account;

    public AuthenticationEvent(Account account) {
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

}
