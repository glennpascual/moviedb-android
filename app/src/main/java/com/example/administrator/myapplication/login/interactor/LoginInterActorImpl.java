package com.example.administrator.myapplication.login.interactor;

import android.os.AsyncTask;

import com.example.administrator.myapplication.login.LoginInteractor;
import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.task.LoginTask;
import com.example.administrator.myapplication.threadpool.TaskManager;

/**
 * Created by Administrator on 7/8/2016.
 */
public class LoginInterActorImpl
    implements LoginInteractor {


    // Task name constants
    private static final String TASK_NAME_LOGIN = "login_task";


    private TaskManager taskManager;

    //
    // LoginTask
    private LoginTask loginTask;
    private LoginTask.Callback loginTaskCallback;


    private LoginInteractor.Callback callback;


    //
    //
    public LoginInterActorImpl(TaskManager taskManager) {
        this.taskManager = taskManager;
    }


    @Override
    public void login(String username, String password, final Callback callback) {
        this.callback = callback;

        // Execute in task manager
        loginTask = (LoginTask) taskManager.getRunningTask(TASK_NAME_LOGIN);

        if(loginTask == null || loginTask.getStatus() == AsyncTask.Status.FINISHED) {

            //
            loginTaskCallback = new LoginTask.Callback() {
                @Override
                public void onLoginStarted(String username) {
                    if(callback != null)
                        callback.onLoginStarted();
                }

                @Override
                public void onLoginCompleted(int messageCode, String username, Account account) {
                    if(callback != null)
                        callback.onLoginCompleted(username, account, messageCode);
                }

                @Override
                public void onLoginError() {
                    if(callback != null)
                        callback.onLoginError();
                }

            };


            loginTask = new LoginTask(username, password);
            loginTask.setCallback(loginTaskCallback);

            // Execute in Task manager
            taskManager.executeTask(TASK_NAME_LOGIN, loginTask);

        }
        else {
            loginTask.setCallback(loginTaskCallback);
        }

    }


}
