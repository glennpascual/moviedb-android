package com.example.administrator.myapplication.login;

import com.example.administrator.myapplication.model.Account;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface LoginInteractor {

    interface Callback {
        void onLoginStarted();
        void onLoginCompleted(String username, Account account, int message);
        void onLoginError();
    }


    void login(String username, String password, Callback callback);


}
