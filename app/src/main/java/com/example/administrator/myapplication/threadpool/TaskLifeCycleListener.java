package com.example.administrator.myapplication.threadpool;

/**
 * Created by Administrator on 7/4/2016.
 */
interface TaskLifeCycleListener {

    void onCreate(String name);
    void onDeath(String name);
}
