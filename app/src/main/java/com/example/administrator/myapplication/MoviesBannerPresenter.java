package com.example.administrator.myapplication;

import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.BannerFetchTask;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.MovieFetchTask;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.threadpool.TaskManager;

import java.util.List;

/**
 * Created by Administrator on 7/13/2016.
 */
public class MoviesBannerPresenter
    implements MovieBannersContract.Presenter {


    //
    public static final String TASK_NAME_MOVIE_FETCH = "task_name_movie_fetch";
    public static final String TASK_NAME_BANNER_FETCH = "task_name_banner_fetch";


    //
    //
    private MovieBannersContract.View view;

    //
    private TaskManager taskManager;

    //
    private MovieFetchTask movieFetchTask;

    //
    private BannerFetchTask bannerFetchTask;


    public MoviesBannerPresenter(MovieBannersContract.View view, TaskManager taskManager) {
        this.view = view;
        this.taskManager = taskManager;
    }


    @Override
    public void loadMovies() {

        movieFetchTask = new MovieFetchTask();
        movieFetchTask.setCallback(new MovieFetchTask.Callback() {
            @Override
            public void onMovieFetchStarted() {
                if(view != null)
                    view.showProgressBarMovies();
            }

            @Override
            public void onMovieFetchCompleted(List<Movie> movies) {
                if(view != null) {
                    view.hideProgressBarMovies();
                    view.setMovies(movies);
                }
            }

            @Override
            public void onMovieFetchError() {
                if(view != null)
                    view.hideProgressBarMovies();
            }
        });

        // Execute in task manager
        taskManager.executeTask(TASK_NAME_MOVIE_FETCH, movieFetchTask);
    }

    @Override
    public void loadBanners() {

        bannerFetchTask = new BannerFetchTask();
        bannerFetchTask.setCallback(new BannerFetchTask.Callback() {

            @Override
            public void onBannerFetchStarted() {
                if(view != null)
                    view.showProgressBarBanners();
            }

            @Override
            public void onBannerFetchCompleted(List<Banner> movies) {

                if(view != null) {
                    view.setBanners(movies);
                    view.hideProgressBarBanners();
                }
            }

            @Override
            public void onBannerFetchError() {
                if(view != null) {
                    view.hideProgressBarBanners();
                }
            }

        });

        // Execute in task manager
        taskManager.executeTask(TASK_NAME_BANNER_FETCH, bannerFetchTask);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

}
