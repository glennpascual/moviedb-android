package com.example.administrator.myapplication.model;

import com.example.administrator.myapplication.query.MovieDBOrgDataSource;
import com.example.administrator.myapplication.query.MoviesQuery;
import com.example.administrator.myapplication.threadpool.ManagedAsyncTask;

import java.util.List;

/**
 * Created by Administrator on 7/6/2016.
 */ //
// Task classes
//
public class MovieFetchTask
        extends ManagedAsyncTask<Void, List<Movie>> {

    //
    private UserSession userSession;

    //
    private Callback internalCallback;

    //
    private Callback callback;

    //
    public interface Callback {
        void onMovieFetchStarted();

        void onMovieFetchCompleted(List<Movie> movies);

        void onMovieFetchError();
    }


    //
    public MovieFetchTask() {
    }

    void setInternalCallback(Callback internalCallback) {
        this.internalCallback = internalCallback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (internalCallback != null) {
            internalCallback.onMovieFetchStarted();
        }

        if (callback != null) {
            callback.onMovieFetchStarted();
        }

    }


    @Override
    public List<Movie> standByDoInBackground(Object... objects) {

        try {
            MoviesQuery moviesQuery = new MovieDBOrgDataSource();
            return moviesQuery.fetchMovies();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    protected void onPostExecute(List<Movie> movies) {
        super.onPostExecute(movies);

        if (internalCallback != null) {

            if (movies != null) {



                internalCallback.onMovieFetchCompleted(movies);
            }
            else
                internalCallback.onMovieFetchError();

        }

        if (callback != null) {

            if (movies != null)
                callback.onMovieFetchCompleted(movies);
            else
                callback.onMovieFetchError();

        }

    }


}
