package com.example.administrator.myapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.administrator.myapplication.MainActivity;
import com.example.administrator.myapplication.MovieDBApplication;
import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.events.SignUpEvent;
import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.signup.SignUpPresenter;
import com.example.administrator.myapplication.signup.SignUpView;
import com.example.administrator.myapplication.signup.presenter.SignUpPresenterImpl;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Administrator on 7/15/2016.
 */
public class SignUpFragment
    extends BaseFragment
        implements SignUpView {


    // Request code constants
    private static final int REQUESTCODE_SIGN_UP = 1;


    // Task name constants
    private static final String TASK_NAME_LOGIN = "login_task";


    // Edit Text
    @BindView(R.id.email) EditText editTextUsername;
    @BindView(R.id.password) EditText editTextPassword;
    @BindView(R.id.login_progress) ProgressBar progressBar;


    // Buttons
    @BindView(R.id.email_sign_up_button) Button signUpButton;

    // Presenter
    private SignUpPresenter signUpPresenter;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_signup, container, false);
        ButterKnife.bind(this, content);

        return content;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MovieDBApplication movieDBApplication = MovieDBApplication.from(getContext());
        signUpPresenter = new SignUpPresenterImpl(this, getContext(), movieDBApplication.getApplicationTaskManager());

    }


    @OnClick(R.id.email_sign_up_button)
    public void signUp() {
        signUpPresenter.signUp(getUsername(), getPassword(), null);
    }

    //
    // SignUpView implementation


    @Override
    public String getUsername() {
        return editTextUsername.getText().toString();
    }

    @Override
    public String getPassword() {
        return editTextPassword.getText().toString();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setUserNameErrorMessage(String message) {
        editTextUsername.setError(message);
    }

    @Override
    public void setPasswordErrorMessage(String message) {
        editTextPassword.setError(message);
    }

    @Override
    public void finishSignIn(Account account) {


        //
        ((MainActivity) getActivity()).popNavigation(this);

        EventBus.getDefault().post(new SignUpEvent(account));

    }
}
