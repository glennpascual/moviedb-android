package com.example.administrator.myapplication;

import android.support.v7.app.AppCompatActivity;

import com.example.administrator.myapplication.model.UserSession;

/**
 * Created by Administrator on 7/4/2016.
 */
public class MovieDBActivity
    extends AppCompatActivity {




    protected UserSession getCurrentUserSession() {
        MovieDBApplication movieDBApplication = MovieDBApplication.from(this);
        return movieDBApplication.getCurrentUserSession();
    }


}
