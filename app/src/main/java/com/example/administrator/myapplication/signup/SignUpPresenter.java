package com.example.administrator.myapplication.signup;

/**
 * Created by Administrator on 7/8/2016.
 */
public interface SignUpPresenter {

    void signUp(String username, String password, String email);
    void onDestroy();

}
