package com.example.administrator.myapplication.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.administrator.myapplication.model.UserSession;

/**
 * Created by Administrator on 7/8/2016.
 */
public abstract class UserSessionBroadcastReceiver
    extends BroadcastReceiver {


    public static final int USER_SESSION_REMOVED = -1;
    public static final int USER_SESSION_CHANGED = -1;


    @Override
    public final void onReceive(Context context, Intent intent) {
        
    }


    protected abstract void onUserSessionChanged(UserSession userSession);
    protected abstract void onUserSessionRemoved(UserSession userSession);


}
