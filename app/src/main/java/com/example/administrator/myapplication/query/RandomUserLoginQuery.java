package com.example.administrator.myapplication.query;

import android.util.JsonReader;

import com.example.administrator.myapplication.model.Account;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Administrator on 7/8/2016.
 */
public class RandomUserLoginQuery
    extends LoginQuery {


    private static final String URL_LOGIN = "http://api.randomuser.me/?format=json&inc=gender,name,location,email,picture";


    // Json Names
    private static final String JSON_RESULTS = "results";

    private static final String JSON_NAME = "name";
    private static final String JSON_NAME_FIRST = "first";
    private static final String JSON_NAME_LAST = "last";
    private static final String JSON_NAME_EMAIL = "email";

    private static final String JSON_PICTURE = "picture";
    private static final String JSON_PICTURE_LARGE = "large";
    private static final String JSON_PICTURE_MEDIUM = "medium";
    private static final String JSON_PICTURE_THUMBNAIL = "thumbnail";

    private static final String JSON_PICTURE_LOCATION = "location";
    private static final String JSON_PICTURE_STREET = "street";
    private static final String JSON_PICTURE_CITY = "city";
    private static final String JSON_PICTURE_STATE = "state";
    private static final String JSON_PICTURE_POSTCODE = "postcode";


    private static final String JSON_INFO = "info";
    private static final String JSON_SEED = "seed";



    @Override
    public Account login(String username, String password)
            throws Exception {


        URL url = new URL(URL_LOGIN);

        HttpURLConnection httpURLConnection = null;
        InputStream inputStream = null;

        JsonReader jsonReader = null;

        try {

            httpURLConnection = (HttpURLConnection) url.openConnection();
            inputStream = httpURLConnection.getInputStream();

            jsonReader = new JsonReader(new InputStreamReader(inputStream, "utf-8"));

            jsonReader.beginObject();


            //
            // Account fields
            String token = null;

            String pictureURL = null;
            String address = null;
            String fullName = null;
            String email = null;

            Account account = null;






            String name = null;
            while(jsonReader.hasNext()) {

                name = jsonReader.nextName();

                if(JSON_RESULTS.equalsIgnoreCase(name)) {


                    // [
                    jsonReader.beginArray();

                    while(jsonReader.hasNext()) {


                        // {
                        jsonReader.beginObject();

                        while(jsonReader.hasNext()) {

                            name = jsonReader.nextName();

                            if (JSON_NAME.equalsIgnoreCase(name)) {

                                // {
                                jsonReader.beginObject();

                                String first = null;
                                String last = null;

                                while (jsonReader.hasNext()) {

                                    name = jsonReader.nextName();

                                    if (JSON_NAME_FIRST.equalsIgnoreCase(name)) {

                                        first = jsonReader.nextString();
                                    } else if (JSON_NAME_LAST.equalsIgnoreCase(name)) {
                                        last = jsonReader.nextString();
                                    } else {
                                        jsonReader.skipValue();
                                    }

                                }

                                fullName = String.format("%s %s", first, last);


                                // }
                                jsonReader.endObject();

                            } else if (JSON_PICTURE.equalsIgnoreCase(name)) {

                                jsonReader.beginObject();

                                while (jsonReader.hasNext()) {

                                    name = jsonReader.nextName();
                                    if (JSON_PICTURE_LARGE.equalsIgnoreCase(name)) {
                                        pictureURL = jsonReader.nextString();
                                    } else {
                                        jsonReader.skipValue();
                                    }

                                }

                                jsonReader.endObject();

                            } else if (JSON_NAME_EMAIL.equalsIgnoreCase(name)) {

                                email = jsonReader.nextString();

                            } else if (JSON_PICTURE_LOCATION.equalsIgnoreCase(name)) {

                                // {
                                jsonReader.beginObject();

                                String street = null;
                                String city = null;
                                String state = null;
                                String postcode = null;

                                while (jsonReader.hasNext()) {

                                    name = jsonReader.nextName();

                                    if (JSON_PICTURE_STREET.equalsIgnoreCase(name)) {
                                        street = jsonReader.nextString();
                                    } else if (JSON_PICTURE_CITY.equalsIgnoreCase(name)) {
                                        city = jsonReader.nextString();
                                    } else if (JSON_PICTURE_STATE.equalsIgnoreCase(name)) {
                                        state = jsonReader.nextString();
                                    } else if (JSON_PICTURE_POSTCODE.equalsIgnoreCase(name)) {
                                        postcode = jsonReader.nextString();
                                    } else {
                                        jsonReader.skipValue();
                                    }

                                }

                                // }
                                jsonReader.endObject();

                                address = String.format("%s %s %s %s", postcode, street, state, city);


                            } else {
                                jsonReader.skipValue();
                            }

                        }

                        // }
                        jsonReader.endObject();


                    }




                    // ]
                    jsonReader.endArray();

                }
                else {
                    jsonReader.skipValue();
                }


            }


            jsonReader.endObject();


            //
            //
            account = new Account(username, Integer.toHexString(username.hashCode()));
            account.setUserImageURI(pictureURL);
            account.setAddress(address);
            account.setFullName(fullName);

            return account;

        }
        finally {

            if(inputStream != null) {

                try {
                    inputStream.close();
                }
                catch (Exception e) {
                }

            }

            //
            //

        }

    }
}
