package com.example.administrator.myapplication.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.model.Banner;
import com.squareup.picasso.Picasso;

/**
 * Created by Administrator on 7/5/2016.
 */
public class BannerFragment
    extends BaseFragment {


    // Banner's image
    private ImageView imageView;


    // Banner displayed
    private Banner banner;

    // Listener
    // TODO WeakReference
    private Listener listener;

    private Picasso picasso;


    public interface Listener {
        void onBannerClicked(Banner banner);
    }

    public void setBanner(Banner banner) {
        this.banner = banner;

        if(imageView != null) {
            populateBanner(banner);
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;

    }

    private void populateBanner(Banner banner) {
        picasso.load(banner.getMovieBanner().getMoviePosterURI()).into(imageView);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        picasso = Picasso.with(context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        picasso = Picasso.with(activity);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(banner != null)
            populateBanner(banner);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View content = inflater.inflate(R.layout.fragment_banner, container, false);

        imageView = (ImageView) content.findViewById(R.id.imageViewBanner);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null)
                    listener.onBannerClicked(banner);
            }
        });

        return content;
    }

}
