package com.example.administrator.myapplication.model;

/**
 * Created by Administrator on 7/4/2016.
 */
public class ApplicationState {

    private UserSession currentUser;


    public UserSession getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserSession currentUser) {
        this.currentUser = currentUser;
    }
}
