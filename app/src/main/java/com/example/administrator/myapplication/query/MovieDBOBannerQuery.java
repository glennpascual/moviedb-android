package com.example.administrator.myapplication.query;

import android.util.JsonReader;

import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 7/5/2016.
 */
public class MovieDBOBannerQuery
    extends BannerQuery {


    //
    private static final String URL_FETCH = "https://api.themoviedb.org/3/discover/movie?api_key=aa01ca1c073058a4dab22bf495ce7fc4";


    // Movie Image String
    private static final String MOVIE_IMAGE_URL = "http://image.tmdb.org/t/p/w500%s";


    // Release Date format
    private static final DateFormat RELEASE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");


    // Json Object/Array names
    private static final String JSON_MOVIE_TITLE = "title";
    private static final String JSON_RESULTS = "results";
    private static final String JSON_RELEASE_DATE = "release_date";
    private static final String JSON_POSTER_PATH = "backdrop_path";
    private static final String JSON_MOVIE_ID = "id";
    private static final String JSON_MOVIE_OVERVIEW = "overview";


    @Override
    public List<Banner> queryBanners()
            throws Exception {

        URL url = new URL(URL_FETCH);

        // Open Connection
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.connect();

        // Content InputStream
        InputStream inputStream = null;

        List<Banner> bannerList = null;

        try {

            bannerList = new ArrayList<>();

            inputStream = httpURLConnection.getInputStream();

            JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));


            jsonReader.beginObject();

            // Movie object fields
            String title = null;
            Date releaseDate = null;

            String posterUrl = null;
            String overview = null;

            int id = -1;


            //
            Banner banner = null;
            Movie parsedMovie = null;

            //
            // Parsing fields

            String name = null;
            while (jsonReader.hasNext()) {

                name = jsonReader.nextName();

                if(JSON_RESULTS.equalsIgnoreCase(name)) {

                    jsonReader.beginArray();


                    while (jsonReader.hasNext()) {

                        jsonReader.beginObject();

                        while(jsonReader.hasNext()) {

                            name = jsonReader.nextName();

                            //
                            if (JSON_RELEASE_DATE.equalsIgnoreCase(name)) {
                                releaseDate = RELEASE_DATE_FORMAT.parse(jsonReader.nextString());
                            }
                            //
                            else if (JSON_MOVIE_ID.equalsIgnoreCase(name)) {
                                id = jsonReader.nextInt();
                            }

                            //
                            else if (JSON_MOVIE_TITLE.equalsIgnoreCase(name)) {
                                title = jsonReader.nextString();
                            }
                            else if (JSON_MOVIE_OVERVIEW.equalsIgnoreCase(name)) {
                                overview = jsonReader.nextString();
                            }

                            //
                            else if (JSON_POSTER_PATH.equalsIgnoreCase(name)) {
                                posterUrl = String.format(MOVIE_IMAGE_URL, jsonReader.nextString());
                            }
                            //
                            //
                            else {
                                jsonReader.skipValue();
                            }

                        }

                        jsonReader.endObject();

                        parsedMovie = new Movie();
                        parsedMovie.setTitle(title);
                        parsedMovie.setId(id);
                        parsedMovie.setDateReleased(releaseDate);
                        parsedMovie.setMoviePosterURI(posterUrl);
                        parsedMovie.setOverView(overview);

                        //
                        bannerList.add(new Banner(parsedMovie));

                    }

                    jsonReader.endArray();



                }
                else {
                    jsonReader.skipValue();
                }

            }


            //
            jsonReader.endObject();

            jsonReader.close();

            return bannerList;
        }
        finally {

            if(inputStream != null) {

                try {
                    inputStream.close();
                }
                catch (Exception e) {}

            }


        }

    }


}
