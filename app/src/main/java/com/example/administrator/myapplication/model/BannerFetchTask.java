package com.example.administrator.myapplication.model;

import com.example.administrator.myapplication.query.BannerQuery;
import com.example.administrator.myapplication.query.MovieDBOBannerQuery;
import com.example.administrator.myapplication.threadpool.ManagedAsyncTask;

import java.util.List;

/**
 * Created by Administrator on 7/6/2016.
 */
public class BannerFetchTask
        extends ManagedAsyncTask<Void, List<Banner>> {

    //
    private Callback internalCallback;

    //
    private Callback callback;

    public interface Callback {
        void onBannerFetchStarted();

        void onBannerFetchCompleted(List<Banner> movies);

        void onBannerFetchError();
    }


    //
    public BannerFetchTask() {
    }

    void setInternalCallback(Callback internalCallback) {
        this.internalCallback = internalCallback;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (internalCallback != null) {
            internalCallback.onBannerFetchStarted();
        }

        if (callback != null) {
            callback.onBannerFetchStarted();
        }

    }


    @Override
    public List<Banner> standByDoInBackground(Object... objects) {


        try {

            //
            BannerQuery movieDataSource = new MovieDBOBannerQuery();

            //
            return movieDataSource.queryBanners();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }


    @Override
    protected void onPostExecute(List<Banner> banners) {
        super.onPostExecute(banners);

        if (internalCallback != null) {

            if (banners != null)
                internalCallback.onBannerFetchCompleted(banners);
            else
                internalCallback.onBannerFetchError();

        }

        if (callback != null) {

            if (banners != null)
                callback.onBannerFetchCompleted(banners);
            else
                callback.onBannerFetchError();

        }

    }

}
