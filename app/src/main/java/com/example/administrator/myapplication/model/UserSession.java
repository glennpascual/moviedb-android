package com.example.administrator.myapplication.model;

import java.util.List;

/**
 * Created by Administrator on 7/4/2016.
 */
public class UserSession {


    public static final String TASK_NAME_MOVIE_FETCH = "task_name_movie_fetch";
    public static final String TASK_NAME_BANNER_FETCH = "task_name_banner_fetch";


    //
    private Account account;


    // List of available movies to this user
    private List<Movie> movieList;

    // List of banners
    private List<Banner> banners;


    public UserSession(Account account, List<Movie> movieList, List<Banner> banners) {

        this.account = account;
        this.movieList = movieList;
        this.banners = banners;

    }


    public void setAccount(Account account) {
        this.account = account;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }

    public Account getAccount() {
        return account;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public List<Banner> getBanners() {
        return banners;
    }
}
