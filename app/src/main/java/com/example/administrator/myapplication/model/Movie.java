package com.example.administrator.myapplication.model;

import java.util.Date;

/**
 * Created by Administrator on 7/4/2016.
 */
public class Movie {


    //
    private int id;

    // Title of movie
    private String title;

    // Category of Movie
    // private List<String> category;
    private String category;

    // Date of release
    private Date dateReleased;

    //
    private int rating;

    // Movie Poster URI
    private String moviePosterURI;

    // Overview/Description
    private String overView;


    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDateReleased(Date dateReleased) {
        this.dateReleased = dateReleased;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void setMoviePosterURI(String moviePosterURI) {
        this.moviePosterURI = moviePosterURI;
    }

    public void setOverView(String overView) {
        this.overView = overView;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public Date getDateReleased() {
        return dateReleased;
    }

    public int getRating() {
        return rating;
    }

    public String getMoviePosterURI() {
        return moviePosterURI;
    }

    public String getOverView() {
        return overView;
    }

    public int getId() {
        return id;
    }

}
