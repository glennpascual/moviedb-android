package com.example.administrator.myapplication.query;

import android.util.JsonReader;
import android.util.JsonToken;

import com.example.administrator.myapplication.model.Movie;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 7/6/2016.
 */
public class MovieDBOrgMovieDetailQuery
    extends MovieDetailQuery {


    //
    private static final String URL = "https://api.themoviedb.org/3/movie/%s?api_key=aa01ca1c073058a4dab22bf495ce7fc4";

    //
    // Movie Image String
    private static final String MOVIE_IMAGE_URL = "http://image.tmdb.org/t/p/w500%s";

    // Json names constants
    private static final String JSON_NAME_MOVIE_TITLE = "title";
    private static final String JSON_RELEASE_DATE = "release_date";
    private static final String JSON_POSTER_PATH = "backdrop_path";
    private static final String JSON_MOVIE_ID = "id";
    private static final String JSON_MOVIE_OVERVIEW = "overview";

    // Release Date format
    private static DateFormat releaseDateFormat = new SimpleDateFormat("yyyy-MM-dd");



    @Override
    public Movie fetchMovie(int movieId) throws Exception {

        URL url = new URL(String.format(URL, movieId));

        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        InputStream inputStreamContent = null;


        try {

            //
            inputStreamContent = httpURLConnection.getInputStream();


            JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStreamContent, "utf-8"));

            //
            // Movie fields
            String title = null;
            int id = -1;
            String overView = null;
            Date releaseDate = null;
            String posterURL = null;

            Movie movie = new Movie();


            // {
            jsonReader.beginObject();

            String jsonName = null;

            // Explore by looping
            while(jsonReader.hasNext()) {

                jsonName = jsonReader.nextName();


                // "title" : <movie title string>
                if(JSON_NAME_MOVIE_TITLE.equalsIgnoreCase(jsonName)) {

                    JsonToken peekTitleValue = jsonReader.peek();

                    if(peekTitleValue == JsonToken.STRING)
                        title = jsonReader.nextString();
                    else
                        jsonReader.skipValue();

                }

                // "id" : <movie id integer>
                else if(JSON_MOVIE_ID.equalsIgnoreCase(jsonName)) {

                    JsonToken peekTitleValue = jsonReader.peek();

                    if(peekTitleValue == JsonToken.NUMBER)
                        id = jsonReader.nextInt();
                    else
                        jsonReader.skipValue();


                }

                // "overview" : <overview string>
                else if(JSON_MOVIE_OVERVIEW.equalsIgnoreCase(jsonName)) {

                    JsonToken peekTitleValue = jsonReader.peek();

                    if(peekTitleValue == JsonToken.STRING)
                        overView = jsonReader.nextString();
                    else
                        jsonReader.skipValue();

                }

                // "releaseDate" : <release date string yyyy-mm-dd >
                else if(JSON_RELEASE_DATE.equalsIgnoreCase(jsonName)) {

                    JsonToken peekTitleValue = jsonReader.peek();

                    if(peekTitleValue == JsonToken.STRING)
                        releaseDate = releaseDateFormat.parse(jsonReader.nextString());
                    else
                        jsonReader.skipValue();

                }

                // "poster_path" : <poster path url string>
                else if(JSON_POSTER_PATH.equalsIgnoreCase(jsonName)) {

                    JsonToken peekTitleValue = jsonReader.peek();

                    if(peekTitleValue == JsonToken.STRING)
                        posterURL = String.format(MOVIE_IMAGE_URL, jsonReader.nextString()) ;
                    else
                        jsonReader.skipValue();

                }

                else {
                    jsonReader.skipValue();
                }

            }


            jsonReader.endObject();
            // }


            movie = new Movie();
            movie.setTitle(title);
            movie.setOverView(overView);
            movie.setDateReleased(releaseDate);
            movie.setMoviePosterURI(null);
            movie.setId(id);
            movie.setMoviePosterURI(posterURL);


            return movie;
        }
        finally {

            //
            if(inputStreamContent != null) {

                try {
                    inputStreamContent.close();
                }
                catch (Exception e) {

                }

            }


        }

    }


    //
    // Closeable implementation


    @Override
    public void close() throws IOException {

    }
}
