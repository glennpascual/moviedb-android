package com.example.administrator.myapplication.threadpool;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by Administrator on 7/4/2016.
 */
public class TaskManager {


    //
    private ThreadPoolExecutor threadPoolExecutor;

    // Map that contains all running managed async task
    private Map<String, ManagedAsyncTask> managedAsyncTaskMap;

    private TaskLifeCycleListener taskLifeCycleListener;

    public TaskManager(int threadPoolSize) {
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadPoolSize);
        managedAsyncTaskMap = new HashMap<>(threadPoolSize);
    }


    public void executeTask(String name, ManagedAsyncTask managedAsyncTask, Object...params) {

        if(managedAsyncTask == null)
            throw new IllegalArgumentException("Managed asynctask is needed");

        if(taskLifeCycleListener == null) {

            taskLifeCycleListener = new TaskLifeCycleListener() {
                @Override
                public void onCreate(String name) {
                }

                @Override
                public void onDeath(String name) {
                    managedAsyncTaskMap.remove(name);
                }
            };
        }


        managedAsyncTask.setTaskName(name);
        managedAsyncTask.setTaskLifeCycleListener(taskLifeCycleListener);
        managedAsyncTask.executeOnExecutor(threadPoolExecutor, params);
        managedAsyncTaskMap.put(name, managedAsyncTask);

    }


    public ManagedAsyncTask getRunningTask(String name) {
        return managedAsyncTaskMap.get(name);
    }

}
