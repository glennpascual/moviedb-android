package com.example.administrator.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.administrator.myapplication.MovieDBApplication;
import com.example.administrator.myapplication.R;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.movie.detail.MovieDetailContract;
import com.example.administrator.myapplication.movie.detail.MovieDetailPresenter;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Administrator on 7/19/2016.
 */
public class MovieDetailFragment
    extends BaseFragment
        implements MovieDetailContract.View {



    // Extra constants
    public static final String EXTRA_MOVIEID = "extra_movieid";

    // State constants
    public static final String STATE_MOVIEID = "state_movieid";

    //
    private int movieID;


    //
    // Views
    @BindView(R.id.imageViewMoviePoster) ImageView imageViewMoviePoster;

    @BindView(R.id.textViewTitle) TextView textViewTitle;
    @BindView(R.id.textViewOverview) TextView textViewOverView;
    @BindView(R.id.textViewReleaseDate) TextView textViewReleaseDate;


    // Details Container
    @BindView(R.id.detailsContainer) View detailsContainer;

    // Progress indicator view
    @BindView(R.id.progressIndicator) View viewProgressIndicator;

    // Picasso
    private Picasso picasso;

    //
    // Presenter
    private MovieDetailContract.Presenter presenter;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            Bundle arguments = getArguments();
            movieID = arguments.getInt(EXTRA_MOVIEID);
        }
        else {
            movieID = savedInstanceState.getInt(STATE_MOVIEID);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        picasso = Picasso.with(getContext());

        //
        View content = inflater.inflate(R.layout.fragment_movie_detail, container, false);
        ButterKnife.bind(this, content);


        return content;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        MovieDBApplication movieDBApplication = MovieDBApplication.from(getContext());

        presenter = new MovieDetailPresenter(this, movieDBApplication.getApplicationTaskManager());
        presenter.loadMovie(movieID);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_MOVIEID, movieID);
    }

    //
    // MovieDetailContract.View

    private static DateFormat releaseDateDisplayFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void showProgress() {
        viewProgressIndicator.setVisibility(View.VISIBLE);
        detailsContainer.setVisibility(View.GONE);
    }

    @Override
    public void setMovie(Movie movie) {


        picasso.load(movie.getMoviePosterURI()).into(imageViewMoviePoster);

        textViewTitle.setText(movie.getTitle());
        textViewOverView.setText(movie.getOverView());
        textViewReleaseDate.setText(releaseDateDisplayFormat.format(movie.getDateReleased()));
    }

    @Override
    public void hideProgress() {
        viewProgressIndicator.setVisibility(View.GONE);
        detailsContainer.setVisibility(View.VISIBLE);
    }


}
