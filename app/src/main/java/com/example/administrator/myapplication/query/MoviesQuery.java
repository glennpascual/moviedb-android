package com.example.administrator.myapplication.query;

import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;

import java.util.List;

/**
 * Created by Administrator on 7/4/2016.
 */
public abstract class MoviesQuery {

    abstract public List<Movie> fetchMovies() throws Exception;

}
