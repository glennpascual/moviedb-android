package com.example.administrator.myapplication.movie.detail;

import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;
import com.example.administrator.myapplication.query.MovieDBOrgMovieDetailQuery;
import com.example.administrator.myapplication.query.MovieDetailQuery;
import com.example.administrator.myapplication.threadpool.ManagedAsyncTask;

/**
 * Created by Administrator on 7/11/2016.
 */
public class MovieDetailFetchTask
        extends ManagedAsyncTask<Void, Movie> {


    // Movie ID to be passsed to http request
    private int movieId;

    //
    private UserSession userSession;

    //
    // Callback
    private Callback callback;

    public interface Callback {
        void onMovieDetailFetchStart();

        void onMovieDetailFetchCompleted(UserSession userSession, Movie movie);

        void onMovieDetailFetchError();
    }


    public MovieDetailFetchTask(int movieId, UserSession userSession) {
        this.movieId = movieId;
        this.userSession = userSession;
    }


    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (callback != null)
            callback.onMovieDetailFetchStart();
    }

    @Override
    public Movie standByDoInBackground(Object... objects) {

        MovieDetailQuery movieDetailQuery = new MovieDBOrgMovieDetailQuery();

        try {
            return movieDetailQuery.fetchMovie(movieId);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }


    }

    @Override
    protected void onPostExecute(Movie movie) {
        super.onPostExecute(movie);

        if (callback != null) {

            if (movie != null)
                callback.onMovieDetailFetchCompleted(userSession, movie);
            else
                callback.onMovieDetailFetchError();

        }

    }
}
