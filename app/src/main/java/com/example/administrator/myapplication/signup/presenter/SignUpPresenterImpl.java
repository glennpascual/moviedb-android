package com.example.administrator.myapplication.signup.presenter;

import android.content.Context;

import com.example.administrator.myapplication.model.Account;
import com.example.administrator.myapplication.signup.SignUpInteractor;
import com.example.administrator.myapplication.signup.SignUpPresenter;
import com.example.administrator.myapplication.signup.SignUpView;
import com.example.administrator.myapplication.signup.interactor.SignUpInteractorImpl;
import com.example.administrator.myapplication.threadpool.TaskManager;

/**
 * Created by Administrator on 7/8/2016.
 */
public class SignUpPresenterImpl
    implements
        SignUpPresenter,
        SignUpInteractor.Callback {



    //
    private SignUpView signUpView;
    private Context context;
    private TaskManager taskManager;


    //
    private SignUpInteractor signUpInteractor;


    public SignUpPresenterImpl(SignUpView signUpView, Context context, TaskManager taskManager) {
        this.signUpView = signUpView;
        this.context = context;
        this.taskManager = taskManager;

        signUpInteractor = new SignUpInteractorImpl(taskManager);
    }

    @Override
    public void signUp(String username, String password, String email) {

        if(username == null || username.isEmpty()) {
            if(signUpView != null)
                signUpView.setUserNameErrorMessage("Username cannot be empty");
            return;
        }

        if(password == null || password.isEmpty()) {

            if(signUpView != null)
                signUpView.setPasswordErrorMessage("Password cannot be empty");
            return;
        }

        signUpInteractor.signUp(username, password, email, this);


    }

    @Override
    public void onDestroy() {
        context = null;
        signUpView = null;
    }

    //
    // SignUpInteractor.Callback implementation

    @Override
    public void onSignUpStarted() {
        signUpView.showProgress();
    }

    @Override
    public void onSignUpCompleted(String username, Account account, int message) {
        if(signUpView != null) {
            signUpView.hideProgress();
            signUpView.finishSignIn(account);
        }
    }

    @Override
    public void onSignUpError() {
        if(signUpView != null)
            signUpView.hideProgress();
    }
}
