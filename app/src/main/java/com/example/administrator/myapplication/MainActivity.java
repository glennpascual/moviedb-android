package com.example.administrator.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.administrator.myapplication.events.LoginEvent;
import com.example.administrator.myapplication.events.SignOutEvent;
import com.example.administrator.myapplication.fragments.BaseFragment;
import com.example.administrator.myapplication.fragments.LoginFragment;
import com.example.administrator.myapplication.fragments.MovieDetailFragment;
import com.example.administrator.myapplication.fragments.MoviesBannersFragment;
import com.example.administrator.myapplication.fragments.MainFragment;
import com.example.administrator.myapplication.model.Banner;
import com.example.administrator.myapplication.model.Movie;
import com.example.administrator.myapplication.model.UserSession;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class MainActivity
        extends MovieDBActivity
        implements
            MoviesBannersFragment.Listener,
            FragmentManager.OnBackStackChangedListener {



    // Request code constants
    private static final int REQUEST_CODE_LOGIN = 1;


    // Tablayout and Toolbar
    private Toolbar toolbar;

    // Menu to control login/logout button
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        setContentView(R.layout.activity_main);

        //
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //
        //
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.addOnBackStackChangedListener(this);

        Log.d("MainActivity", "onCreate()");

        if(savedInstanceState == null) {
            addNavigation(new MainFragment(), false, true);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        this.menu = menu;

        updateMenu();

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int sourceMenuId = item.getItemId();

        switch (sourceMenuId) {

            case android.R.id.home:

                onBackPressed();

                break;

            case R.id.menu_login:

                LoginFragment loginFragment = new LoginFragment();
                loginFragment.setBackStackName("login_fragment");

                addNavigation(loginFragment, true, true);

                break;

            case R.id.menu_logout:

                UserSession userSession = getCurrentUserSession();

                EventBus.getDefault().post(new SignOutEvent(userSession.getAccount()));

                break;

        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);

        Log.d("MainActivity", "onResume()");
    }


    @Override
    protected void onDestroy() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.removeOnBackStackChangedListener(this);

        // Unregister event bus
        EventBus.getDefault().unregister(this);

        super.onDestroy();
        Log.d("MainActivity", "onDestroy()");

    }





    @Override
    protected void onPause() {
        super.onPause();
    }


    //
    // Private methods

    private void updateMenu() {

        // Check if the use has logged out or not
        UserSession userSession = getCurrentUserSession();
        boolean loggedIn = userSession != null;

        menu.findItem(R.id.menu_login).setVisible(!loggedIn);
        menu.findItem(R.id.menu_logout).setVisible(loggedIn);

    }

    public void popNavigation(BaseFragment fragment) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.popBackStack(fragment.getBackStackName(), FragmentManager.POP_BACK_STACK_INCLUSIVE);



    }

    public void addNavigation(BaseFragment fragment, boolean addToBackStack, boolean doNotAddIfExists) {

        FragmentManager fragmentManager = getSupportFragmentManager();



        if(fragmentManager.findFragmentByTag(fragment.getBackStackName()) == null) {


            // Create transaction and add fragment specifying name
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.content, fragment, fragment.getBackStackName());

            //
            if (addToBackStack)
                fragmentTransaction.addToBackStack(fragment.getBackStackName());

            //
            fragmentTransaction.commit();


        }

    }



    //
    // FragmentManager.OnBackStackChangedListener implementation

    @Override
    public void onBackStackChanged() {

        ActionBar supportActionBar = getSupportActionBar();

        FragmentManager fragmentManager = getSupportFragmentManager();

        boolean upButtonEnabled = fragmentManager.getBackStackEntryCount() > 0;

        supportActionBar.setDisplayHomeAsUpEnabled(upButtonEnabled);
        supportActionBar.setDisplayShowHomeEnabled(upButtonEnabled);

    }


    //
    // MoviesBannersFragment.Listener implementation


    @Override
    public void onBannerItemClicked(Banner banner, int position) {
        Movie movieBanner = banner.getMovieBanner();

        Bundle arguments = new Bundle();
        arguments.putInt(MovieDetailFragment.EXTRA_MOVIEID, movieBanner.getId());

        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.setArguments(arguments);

        addNavigation(movieDetailFragment, true, true);
    }

    @Override
    public void onMovieItemClicked(Movie movie, int position) {


        Bundle arguments = new Bundle();
        arguments.putInt(MovieDetailFragment.EXTRA_MOVIEID, movie.getId());

        MovieDetailFragment movieDetailFragment = new MovieDetailFragment();
        movieDetailFragment.setArguments(arguments);

        addNavigation(movieDetailFragment, true, true);


    }


    //
    // Event Buss

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLoginEvent(LoginEvent loginEvent) {

        List<Movie> movies = new ArrayList<>();
        List<Banner> banners = new ArrayList<>();

        //
        MovieDBApplication movieDBApplication = MovieDBApplication.from(this);
        movieDBApplication.setCurrentUserSession(new UserSession(loginEvent.getAccount(), movies, banners));

        updateMenu();


    }

    @Subscribe(threadMode =  ThreadMode.MAIN)
    public void onSignOutEvent(SignOutEvent signOutEvent) {

        MovieDBApplication movieDBApplication = MovieDBApplication.from(this);
        movieDBApplication.setCurrentUserSession(null);

        updateMenu();

    }



    //
    //
    // Inner classes

    //
    ////

}
